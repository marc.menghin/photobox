# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Util::DebugHelper
  def printInfo(obj)
    puts "Type: "
    printType(obj)

    puts "Classes & Constants: "
    printClasses(obj)

    puts "Singletons: "
    printSingletons(obj)

    puts "Methods & Properties: "
    printMethods(obj)

    puts "Value: "
    if obj.kind_of?(Hash)
      printHash(obj)
    elsif obj.kind_of?(Array)
      printArray(obj)
    else
      puts "#{obj.to_s}"
    end

    puts obj.inspect
  end

  def printType(obj)
    puts "'#{obj.class.name}' (\##{obj.object_id})"
  end

  def printHash(obj)
    obj.each { |key, value|
      puts " » #{key} => #{value}"
    }
  end

  def printArray(obj)
    obj.each { |value|
      puts " » #{value}"
    }
  end

  def printMethods(obj)
    obj.public_methods.each { |value|
      parameters = obj.method(value).parameters

      if (parameters.count > 0)
        parameters.each { |parameter|
          puts " » #{value}(#{parameter.join(', ')})"
        }
      else
        puts " » #{value}()"
      end
    }
  end

  def printSingletons(obj)
    obj.singleton_methods.each { |value|
      puts " » #{value}"
    }
  end

  def printClasses(obj)
    if (obj.respond_to? :constants)
      obj.constants.each { |value|
        puts " » #{value}"
      }
    else
      puts " » (NONE)"
    end
  end

  module_function :printType, :printHash, :printArray, :printMethods, :printSingletons, :printClasses, :printInfo
end