#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'rubygems'
require 'bundler/setup'
require 'fileutils'
require 'optparse'

#full path of start.rb file
$CLIENTPATH = File.expand_path(File.dirname(__FILE__))

#add start.rb path to ruby search path if not already there
#this prevents errors if sopran is not started from the directory where
#start.rb is located.
if (not ($:.include?($CLIENTPATH) || $:.include?(File.dirname(__FILE__))))
  $: << $CLIENTPATH
end

#user home directory
# linux -> ENV HOME
# windows -> ENV HOMEDRIVE & HOMEPATH
# mac -> ??? (TODO: check on a mac)
$USERHOME = ENV["HOME"]
$USERHOME = $USERHOME != nil ? $USERHOME : (ENV["HOMEDRIVE"] + ENV["HOMEPATH"])
$USERHOME = $USERHOME != nil ? $USERHOME : "." #default fallback

#simplify photobox debugging
$DS = $DEBUGPHOTOBOX ? $DEBUGPHOTOBOX : $DEBUG

require 'photobox'


# Main program entry point
main = Photobox::PhotoboxMain.new
main.run

