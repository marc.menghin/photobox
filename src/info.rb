# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#client information

module PhotoboxInfo
  CLIENTNAME = "Photobox"
  CLIENTNAMESHORT = "Photobox"
  CLIENTDIRECTORYNAME = "photobox"

  #verion number
  CLIENTVERSION = "v0.1"

  CLIENTWEB = "http://marcmenghin.gamedev.at"
  CLIENTDESCRIPTION = "A simple Photobox."
  CLIENTCONTRIBUTORS = "Marc Menghin <marc.menghin@gmail.com>"
end
