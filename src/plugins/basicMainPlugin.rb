# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Photobox::Plugins
  class BasicMainPlugin < Photobox::Plugins::BasicPlugin

    def run
      puts "Run not implemented in '#{self.class().name}'."
    end

    def quit
      puts "quit not implemented in '#{self.class().name}'."
    end

  end
end