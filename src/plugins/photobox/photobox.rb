#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'singleton'

module Photobox::Plugins::Photobox
  class Photobox
    include Singleton
    @logger = nil
    @webcamManager = nil
    @egpmController = nil
    @glibMainloop = nil
    @pluginContext = nil
    @gpioController = nil

    def initialize
      require "gtk3"
      require 'plugins/EG_PM/controller'
      require 'plugins/gpio/controller'
      require 'plugins/gnome/webcam/webcamManager'
      require 'plugins/gnome/photoboxUI/photobox_application'

      @logger = Logging.logger[self]
      @webcamManager = ::Photobox::Plugins::Webcam::WebcamManager.instance
      @egpmController = ::Photobox::Plugins::Egpm::Controller.instance
      @gpioController = ::Photobox::Plugins::Gpio::Controller.instance
      @gtkApp = ::Photobox::Plugins::PhotoboxUI::PhotoboxApplication.new
    end

    def startup (pluginContext)
      @pluginContext = pluginContext

      portids = @egpmController.ports(@egpmController.devices[0])

      #switch all off
      portids.each { |id|
        if (@egpmController.on?(id))
          @egpmController.off!(id)
        end
      }

      @glibMainloop = GLib::MainLoop.new(nil, true)
    end

    def take_picture
      stop_waiting
      @logger.info "Starting Taking Picture Process"

	@webcamManager.show_text("&#9825;")

      portids = @egpmController.ports(@egpmController.devices[0])

      #switch all on
      portids.each { |id|
        if (!@egpmController.on?(id))
          @egpmController.on!(id)
        end
      }

      sleep 1 #let cam startup and focus

      #contdown to 0
      (0..4).each { |i|
        @gpioController.light_toggle
        sleep 1
        @webcamManager.show_text((5 - i).to_s)
      }
      @gpioController.light_toggle
      sleep 1
      @webcamManager.show_text("&#9786; &#9825; &#9786;")

      @webcamManager.take_picture
      @gpioController.light_toggle

      sleep 3 #show picture for few seconds

      @gpioController.light_on

      #switch all off
      portids.each { |id|
        if (@egpmController.on?(id))
          @egpmController.off!(id)
        end
      }
      @logger.info "End of Taking Picture Process"

      start_waiting
    end

    def start_waiting
      stop_waiting

      @gpioController.light_on

      @btnLightThread = Thread.new {
        while true
          sleep 10
          @logger.info "Idle Toggling Light Off"
          @gpioController.light_off
          sleep 1
          @logger.info "Idle Toggling Light On"
          @gpioController.light_on
        end
      }

      #Util::DebugHelper::printInfo(@glibMainloop)

      @btnPressedThread = Thread.new {
        while !@gpioController.pressed?
          sleep 0.2
        end
        @logger.info "Button Pressed"
        GLib::Timeout.add(0) {
          take_picture
          false #removes timeout
	 }
      }
    end

    def stop_waiting
      if @btnLightThread != nil
        @btnLightThread.exit
        @btnLightThread = nil
      end

      if @btnPressedThread != nil
        @btnPressedThread.exit
        @btnPressedThread = nil
      end
    end

    def run

      @webcamManager.start

	start_waiting
	
      #exitAfterSeconds(10)
      #takePictureAfterSeconds(1)

      #exitAfterSeconds(15)
      #takePictureAfterSeconds(5)

      begin
        @glibMainloop.run
      rescue => error
        @logger.error error
      ensure
        @webcamManager.stop
        stop_waiting
      end
    end

    def quit
      @webcamManager.stop
      @glibMainloop.quit
    end

    def takePictureAfterSeconds(sec)
      i = 0
      GLib::Timeout.add(1000) {
        i += 1
        if i > sec
          take_picture
          false #removes timeout
        else
          true #keeps timeout
        end
      }
    end

    def exitAfterSeconds(sec)
      i = 0
      GLib::Timeout.add(1000) {
        i += 1
        if i > sec
          @logger.info "Timeout reached, exiting."
          @pluginContext.exitPlatform
          false #removes timeout
        else
          true #keeps timeout
        end
      }
    end
  end
end
