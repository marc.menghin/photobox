#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#global requires
require 'plugins/basicMainPlugin'

#local requires

module Photobox::Plugins::Photobox
  class Plugin < Photobox::Plugins::BasicMainPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))

    def initialize
      super()
      @name = "Photobox Controller"
      @version = "v0.1"
      @description = "Controlls the Photobox flow."
      @id = :pluginPhotobox
    end

    def startup (pluginContext)
      require 'plugins/gpio/controller'
      require 'plugins/gnome/photoboxUI/photobox_application'
      require 'plugins/EG_PM/controller'
      require 'plugins/gnome/webcam/webcamManager'

      require 'plugins/photobox/photobox'
      @photobox = Photobox.instance
      @photobox.startup(pluginContext)

      return true
    end

    def shutdown (pluginContext)
    end

    def run
      @photobox.run
    end

    def quit
      @photobox.quit
    end
  end
end
