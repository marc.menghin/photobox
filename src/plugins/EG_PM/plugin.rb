#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#global requires
require 'plugins/basicPlugin'

#local requires

module Photobox::Plugins::Egpm
	class Plugin < Photobox::Plugins::BasicPlugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "EG-PM Controller"
			@version = "v0.1"
			@description = "Controller for Energenie Programmable power sockets like the EG-PM2 or EG-PMS2."
			@id = :pluginEgpm
		end
		
		def startup (pluginContext)
			require 'plugins/EG_PM/controller'

			Controller.instance

			return true
		end
		
		def shutdown (pluginContext)
		end		
	end
end
