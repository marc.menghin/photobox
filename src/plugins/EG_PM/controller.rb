#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'singleton'

module Photobox::Plugins::Egpm

  # uses sispm_ctl to control device sockets (http://sispmctl.sourceforge.net/)
  class Controller
    include Singleton

    @logger = nil

    def initialize
      require 'gembird-backend'
      @logger = Logging.logger[self]

      printInformation
    end

    def devices()
      deviceList = Array.new
      i = 0
      GembirdBackend.devices.each { |device|
        deviceList[i] = device[:number]
        i = i + 1
      }
      return deviceList
    end

    def ports(deviceId)
      portList = Array.new
      i = 0
      GembirdBackend.status.each { |socketDevice|
        if (socketDevice[:number] == deviceId)
          socketDevice[:sockets].each { |socket, value|
            portList[i] = socket
            i = i + 1
          }
        end
      }
      return portList
    end

    def on?(portId)
      statusInfo = GembirdBackend.status(portId)
      status = statusInfo.first[:sockets][portId]

      @logger.info "socket #{portId} on? #{status}"
      return status.eql?("on")
    end

    def on!(portId)
      @logger.info "activating socket #{portId}"
      GembirdBackend.on!(portId)
    end

    def off!(portId)
      @logger.info "deactivating socket #{portId}"
      GembirdBackend.off!(portId)
    end

    def printInformation

      @logger.info "SISPM connected device information:"
      GembirdBackend.devices.each { |device|
        @logger.info " » #{device[:number]}[#{device[:usb_device]}-#{device[:usb_bus]}]: #{device[:type]} (#{device[:serial]})"
      }

      @logger.info "SISPM Socket status:"
      GembirdBackend.status.each { |socketDevice|
        @logger.info " » #{socketDevice[:number]}[#{socketDevice[:usb_device]}]:"
        socketDevice[:sockets].each { |socket, value|
          @logger.info "    » #{socket} > #{value}"
        }
      }
    end
  end
end