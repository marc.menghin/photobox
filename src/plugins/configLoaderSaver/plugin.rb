#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#global requires
require 'plugins/basicPlugin'

#local requires
require 'plugins/configLoaderSaver/configLoaderSaver'
	
module Photobox::Plugins::ConfigLoaderSaver
	class Plugin < Photobox::Plugins::BasicPlugin
		PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))
		
		def initialize
			super()
			@name = "Config XML Loader/Saver"
			@version = "v0.1"
			@description = "This plugin loads and saves the configuration to an xml file."
			@id = :pluginConfigLoaderSaver
		end
		
		def startup (pluginContext)
			return true
		end
		
		def load(file, config)
			loader = ConfigLoaderSaver.new(file)
			loader.load(config)
		end
		
		def save(file, config)
			saver = ConfigLoaderSaver.new(file)
			saver.save(config)
		end
		
		def shutdown (pluginContext)
		end		
	end
end
