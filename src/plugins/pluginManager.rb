# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
module Photobox::Plugins
  class PluginManager
    @logger = nil
    @@PLUGIN_FILE = "plugin.rb"

    @@PROFILES = {:photobox => [:pluginPhotobox, :pluginWebcam, :pluginEgpm, :pluginGpio]}
    #@@PROFILES = {:photobox => [:pluginEgpm, :pluginGpio]}

    public

    attr_reader :pluginSearchPaths, :pluginContext

    def initialize
      require 'plugins/basicPlugin'
      require 'plugins/basicMainPlugin'
      require 'plugins/pluginContext'

      @logger = Logging.logger[self]

      @pluginPaths = Array.new
      @pluginSearchPaths = Array.new
      @plugins = Hash.new
      @pluginContext = nil
    end

    def <<(path)
      @pluginSearchPaths << path
    end

    def [](id)
      return @plugins[id]
    end

    def removePluginDirectory(path)
      if @pluginSearchPaths.include?(path)
        @pluginSearchPaths.delete(path)
      end
    end

    def loadPlugins
      loadPluginDirectories()
      loadPluginObjects()
    end

    def exists?(id)
      return @plugins.has_key?(id)
    end

    def existsRunning?(id)
      if (@plugins.has_key?(id))
        plugin = @plugins[id]
        return plugin.running
      end
      return false
    end

    def existsWithClass?(classObj)
      @plugins.each_value { |p|
        if (p.class() == classObj)
          return true
        end
      }
      return false
    end

    def printPluginPaths
      @logger.info "Current list of plugin files:"
      @pluginPaths.each { |path|
        @logger.info " » #{path}"
      }
    end

    def printPlugins
      @logger.info "List of loaded plugins:"
      @plugins.each_value { |obj|
        @logger.info " » #{obj.name} #{obj.version} [#{obj.id} - #{obj.running}]"
      }
    end

    def run
      if @pluginContext.mainObject != nil
        @pluginContext.mainObject.run
      else
        @logger.warn "No registered mainObject found. Client will exit."
      end
    end

    def startup(pluginManager, configManager, cmdOptions)
      @pluginContext = PluginContext.new(pluginManager, configManager, cmdOptions)

      profile = :photobox
      if @@PROFILES.include?(cmdOptions[:profile])
        profile = cmdOptions[:profile]
      end

      disablePlugins()

      @logger.info "Enabling profile: #{profile}"
      @@PROFILES[profile].each { |pid|
        @logger.info " » #{pid}"
      }
      enablePlugins(@@PROFILES[profile])

      @plugins.each_value { |plugin|
        startupPlugin(plugin)
      }
    end

    def shutdown
      @plugins.each_value { |plugin|
        shutdownPlugin(plugin)
      }
      @pluginContext = nil
    end

    private

    def disablePlugins()
      @plugins.each_value { |plugin|
        if (plugin.enabled && plugin.running)
          shutdownPlugin(plugin)
        end

        plugin.enabled = false
      }
    end

    def enablePlugins(pluginIDList)
      @plugins.each_value { |plugin|
        if pluginIDList.include?(plugin.id)
          plugin.enabled = true
        end
      }
    end

    def startupPlugin(plugin)
      if (plugin.enabled && !plugin.running)
        begin
          if (plugin.startup(@pluginContext))
            plugin.running = true
          else
            plugin.running = false
          end
        rescue
          @logger.error "Error in Startup of #{plugin.name}"
          @logger.error $!
          plugin.running = false
        end

        if (plugin.running)
          #check if MainPlugin and register it
          if (plugin.class() < BasicMainPlugin)
            @pluginContext.registerMain(plugin)
          end
        end
      end
    end

    def shutdownPlugin(plugin)
      if (plugin.running)
        begin
          plugin.shutdown(@pluginContext)
          plugin.running = false
        rescue
          @logger.error "Error in Shutdown of #{plugin.name}"
          @logger.error $!
        end
      end
    end

    def loadPluginDirectories
      @pluginSearchPaths.each { |path|
        if (FileTest.directory?(path))

          #add to ruby global search path so plugin requires work
          if (not ($:.include?(path) || File.expand_path(".") == path))
            $: << path
          end

          @logger.info "searching plugins in: #{path}"
          ##list of plugin files within the given path (incl. subdirectories)
          pluginLst = Dir.glob(File.join(path, "**", @@PLUGIN_FILE))
          pluginLst.each { |pfile|
            if (FileTest.exists?(pfile) and FileTest.file?(pfile))
              @logger.info " » #{pfile}"
              @pluginPaths << pfile
            end
          }
        else
          @logger.warn "The plugin path '#{path}' isn't a directory."
        end
      }
    end

	def loadPluginObjects
		@pluginPaths.each { |path|
	        begin
	          load path #allways loads the path (reloads it if already loaded)
	        rescue
	          @logger.warn "Could not load: #{path}"
	          @logger.error $!
	        end
	      }	
	      
	      egpmPlugin = ::Photobox::Plugins::Egpm::Plugin.new
	      gpioPlugin = ::Photobox::Plugins::Gpio::Plugin.new
	      webcamPlugin = ::Photobox::Plugins::Webcam::Plugin.new
	      photoboxPlugin = ::Photobox::Plugins::Photobox::Plugin.new
	      @plugins[egpmPlugin.id] = egpmPlugin
      	      @plugins[gpioPlugin.id] = gpioPlugin
      	      @plugins[webcamPlugin.id] = webcamPlugin
      	      @plugins[photoboxPlugin.id] = photoboxPlugin
	      
	end

    def loadPluginObjects2
      @pluginPaths.each { |path|
        begin
          load path #allways loads the path (reloads it if already loaded)
        rescue
          @logger.warn "Could not load: #{path}"
          @logger.error $!
        end
      }

      regex = Regexp.compile('^Photobox::Plugins::.*::Plugin$')

      ObjectSpace.each_object() { |obj|
        ident = obj.class.to_s
       	name = obj.to_s
        if (ident == "Class" && (regex.match(name)) != nil)
          begin
            if not self.existsWithClass?(obj) #don't load same again
              plugin = obj.new

              if (plugin.class() < BasicPlugin)
                #check if plugin with same id already loaded
                if (self.exists?(plugin.id))
                  @logger.warn "Plugin with ID '#{plugin.id}' already loaded."
                  @logger.warn "  loaded plugin version: #{@plugins[plugin.id].version}"
                  @logger.warn "  new plugin version: #{plugin.version}"
                  @logger.warn "  keeping already loaded plugin."
                else
                  @plugins[plugin.id] = plugin
                end
              end
            else
              @logger.info "Plugin #{name} already loaded."
            end
          rescue
            @logger.warn "Could not load: #{ident} (#{name})"
            @logger.error $!
          end
        end
      }
    end
  end
end

