#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'singleton'

module Photobox::Plugins::Gpio
  class Controller
    include Singleton
    @logger = nil
    @onPi = nil

    def initialize
      @logger = Logging.logger[self]

	@ledPin = 5
    @btnPin = 24
    @btnPinB = 18
@ledStateOn = true
      @onPi = true
      begin
        require 'rpi_gpio'
        RPi::GPIO.set_numbering :board

        RPi::GPIO.setup @btnPin, :as => :input, :pull => :down
        RPi::GPIO.setup @btnPinB, :as => :input, :pull => :down
        RPi::GPIO.setup @ledPin, :as => :output

        ObjectSpace.define_finalizer(self, proc { RPi::GPIO.clean_up })
      rescue
        #not a pi running on
        #@onPi = false
      end
    end

    def light_on
      @logger.info "GPIO: #{@ledPin} ON."
      if (!@onPi)
        return
      end
      RPi::GPIO.set_high @ledPin
      @ledStateOn = true
    end

    def light_off
#      @logger.info "GPIO: #{@ledPin} OFF."
      if (!@onPi)
        return
      end
      RPi::GPIO.set_low @ledPin
      @ledStateOn = false
    end

    def light_toggle
      @logger.info "GPIO: #{@ledPin} TOGGLE."
      if (!@onPi)
        return
      end
      if @ledStateOn
        RPi::GPIO.set_low @ledPin
        @ledStateOn = false
      else
        RPi::GPIO.set_high @ledPin
        @ledStateOn = true
      end
    end

    def pressed?
      #@logger.info "GPIO: #{@btnPin} PRESSED?"
      if (!@onPi)
        return false
      end
      RPi::GPIO.high?(@btnPin) || RPi::GPIO.high?(@btnPinB)
    end
  end
end
