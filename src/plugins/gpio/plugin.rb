#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#global requires
require 'plugins/basicPlugin'

#local requires

# different implementations:
# rpi_gpio > https://github.com/ClockVapor/rpi_gpio
# rpi > https://github.com/jrobertson/rpi
# simple_raspberrypi > https://github.com/jrobertson/simple_raspberrypi
module Photobox::Plugins::Gpio
  class Plugin < Photobox::Plugins::BasicPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))

    def initialize
      super()
      @name = "Raspberry Pi GPIO Pin Controller"
      @version = "v0.1"
      @description = "Controlls the GPIO pins of a Raspberry Pi"
      @id = :pluginGpio
    end

    def startup (pluginContext)
      require 'plugins/gpio/controller'
      Controller.instance

      return true
    end

    def shutdown (pluginContext)
    end
  end
end
