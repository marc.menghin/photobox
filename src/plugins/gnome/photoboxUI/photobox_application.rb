#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'gtk3'

module Photobox::Plugins::PhotoboxUI
  class PhotoboxApplication < Gtk::Application
    @logger = nil
    @builder = nil
    @menuBuilder = nil
    @mainWindow = nil

    def initialize
      super("at.photobox", [:non_unique])

      @logger = Logging.logger[self]

      quit_accels = ["<Ctrl>Q"]
      action = Gio::SimpleAction.new("quit")
      action.signal_connect "activate" do |_action, _parameter|
        quit
      end
      add_action(action)
      set_accels_for_action("app.quit", quit_accels)

      action = Gio::SimpleAction.new("about")
      action.signal_connect "activate" do |_action, _parameter|
        show_about
      end
      add_action(action)

      signal_connect "startup" do |application|
        @builder = Gtk::Builder.new(:file => File.join(Photobox::Plugins::PhotoboxUI::Plugin::PLUGIN_PATH, "uilayouts.ui"))
        @menuBuilder = Gtk::Builder.new(:file => File.join(Photobox::Plugins::PhotoboxUI::Plugin::PLUGIN_PATH, "menu.ui"))
      end

      signal_connect "activate" do |_application|
        begin
          run_application
        rescue => error
          @logger.error "#{error.class}: #{error.message}"
          @logger.error $!
          #error.backtrace.each {|line|
          #  @logger.error "#{line}"
          #}
          @exit_status = 1
          quit
        end
      end
    end

    def run_application
      appmenu = @menuBuilder["appmenu"]
      set_app_menu(appmenu)

      @mainWindow = @builder["PhotoboxMainWindow"]
      add_window(@mainWindow)

      @mainWindow.show_all

      @@mainWindow = @mainWindow #set as global main window
    end

    def show_about
      window = @builder["PhotoboxAbout"]
      add_window(window)

      window.set_transient_for(@mainWindow)
      window.set_destroy_with_parent(true)
      window.show_all
    end
  end
end