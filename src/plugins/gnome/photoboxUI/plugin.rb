#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Photobox::Plugins::PhotoboxUI
  class Plugin < Photobox::Plugins::BasicPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))

    def initialize
      super()
      @name = "Photobox UI"
      @version = "v0.1"
      @description = "Main UI of Photobox"
      @id = :photoboxUI
    end

    def startup (pluginContext)
      begin
        require 'gtk3'
        require 'plugins/gnome/photoboxUI/photobox_application'
        return true
      rescue
        @logger.error "Can't find binding to GTK3."
        @logger.error $!
        return false
      end
    end

    def shutdown (pluginContext)
    end
  end
end
