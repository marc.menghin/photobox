#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)
require 'plugins/basicPlugin'

module Photobox::Plugins::Webcam
  class Plugin < Photobox::Plugins::BasicPlugin
    PLUGIN_PATH = File.expand_path(File.dirname(__FILE__))

    def initialize
      super()
      @name = "Webcam"
      @version = "v0.1"
      @description = "Capture images from Webcam."
      @id = :pluginWebcam
    end

    def startup (pluginContext)
      require 'gst'
      require 'gtk3'

      require 'plugins/gnome/webcam/webcamManager'

      webcamManager = WebcamManager.instance
      webcamManager.startup(pluginContext)

      return true
    end

    def shutdown (pluginContext)
    end
  end
end
