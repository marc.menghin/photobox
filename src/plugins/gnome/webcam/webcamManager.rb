#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

require 'singleton'
require 'time'

module Photobox::Plugins::Webcam
  #Webcam Libs found:
  # rb_webcam -> opencv webcam wrapper https://github.com/TyounanMOTI/rb_webcam
  # ruby-opencv -> directly use obencv bindings https://github.com/ruby-opencv/ruby-opencv/
  # hornetseye-v4l2 -> v4l binding https://github.com/wedesoft/hornetseye-v4l2/
  # gstreamer -> using gstreamer https://rubygems.org/gems/gstreamer
  #
  # using gstreamer for now as it is maintained by same es gtk3 (so up-to-date), others mostly unmaintained

  class WebcamManager
    include Singleton

    @pictureBase = nil
    @mainloop = nil #gtk mainloop
    @pipeline = nil #gst pipeline
    @logger = nil

    def initialize
      require 'gst'
      require 'gtk3'

      @logger = Logging.logger[self]

      @pictureBase = File.join($CLIENTPATH, "..", "..", "img")

      #printGstPlugins
    end

    def startup (pluginContext)
      @pipeline = Gst::Pipeline.new("photobox")
      @webcamsrc = Gst::ElementFactory.make("v4l2src", "v4l2src")
      @webcamsrc.signal_connect("prepare-format") do |element, probability, caps|
        @logger.info "Caps are set to: '#{caps.to_s}'"
      end
      @text = Gst::ElementFactory.make("textoverlay", "textoverlay")
      @tee = Gst::ElementFactory.make("tee", "tee")
      #@videosink = Gst::ElementFactory.make("glimagesink", "glimagesink")
      @fileQueue = Gst::ElementFactory.make("queue", "queueFile")
      @videoQueue = Gst::ElementFactory.make("queue", "queueVideo")
      @valve = Gst::ElementFactory.make("valve", "valve")
      @filesink = Gst::ElementFactory.make("filesink", "filesink")


      # @caps = Gst::caps_from_string("video/x-h264, width=800, height=600, framerate=24/1")
      # @caps = Gst::caps_from_string("video/x-raw-yuv, format=(fourcc)YUY2, width=800, height=600, framerate=24/1")
      #@caps = Gst::caps_from_string("video/x-raw,format=YUY2,width=800,height=600,framerate=24/1")
      #@caps = Gst::caps_from_string("video/x-raw,format=YUY2,width=800,height=600,framerate=24/1")
     #@videosink = Gst::ElementFactory.make("xvimagesink", "xvimagesink")
	@videosink = Gst::ElementFactory.make("ximagesink", "ximagesink")
	#@videosink = Gst::ElementFactory.make("fbdevsink", "fbdevsink")
	#@videosink = Gst::ElementFactory.make("autovideosink", "autovideosink")
	
      @videoConvert = Gst::ElementFactory.make("videoconvert", "videoconvert")
      @videoFlip = Gst::ElementFactory.make("videoflip", "videoflip")
      @capsFilter = Gst::ElementFactory.make("capsfilter", "capsfilter")
      @toJpegConverter = Gst::ElementFactory.make("jpegdec", "jpegdec")
      @toJpegEnc = Gst::ElementFactory.make("jpegenc", "jpegenc")
	@colorSpace = Gst::ElementFactory.make("ffmpegcolorspace", "ffmpegcolorspace")
	@videoScale = Gst::ElementFactory.make("videoscale", "videoscale")

      @glupload = Gst::ElementFactory.make("glupload", "glupload")
      @gleffect = Gst::ElementFactory.make("gleffects", "gleffects")
      @glcolorscale = Gst::ElementFactory.make("glcolorscale", "glcolorscale")

      #Util::DebugHelper::printInfo(@videoFlip)

      @webcamsrc.force_aspect_ratio = true
      @gleffect.hswap = true
      @gleffect.effect = 0

      @text.text = ''
      @text.font_desc = "Sans, 80"
      @text.halignment = 4 # position
      @text.valignment = 3 # position

      #@tee.allow_not_linked = true

      @videoQueue.leaky = 2
      @videoQueue.max_size_buffers = 1
      @fileQueue.leaky = 2
      @fileQueue.max_size_buffers = 1
      @valve.drop = true

      filenamenow = "#{Time.now.utc.iso8601}".gsub(/[:-]/, "_")
      @filesink.location = File.join(@pictureBase, "todelete", "#{filenamenow}.jpeg")
      @filesink.async = false

      # add objects to the main pipeline
      #@pipeline << @webcamsrc << @toJpegConverter << @tee << @videoQueue << @text << @videosink
      #@pipeline << @webcamsrc << @tee << @fileQueue << @valve << @toJpegEnc << @multifilesink
      @pipeline << @webcamsrc << @toJpegConverter << @tee << @videoQueue << @videoScale << @videoConvert << @text << @videosink << @fileQueue << @valve << @toJpegEnc << @filesink

      # link the elements
      #@webcamsrc >> @toJpegConverter >> @tee >> @videoQueue >> @videoScale >> @text >> @videosink
      #@webcamsrc >> @tee >> @fileQueue >> @valve >> @toJpegEnc >> @multifilesink

      @webcamsrc >> @toJpegConverter >> @tee
      @tee >> @videoQueue >> @videoScale >> @text >> @videoConvert >> @videosink
      @tee >> @fileQueue >> @valve >> @toJpegEnc >> @filesink

      @pipeline.bus.add_watch do |bus, message|
        case message.type
          when Gst::MessageType::WARNING
            warning, debug = message.parse_warning
            #@logger.warn "Debugging info: #{debug || 'none'}"
            #@logger.warn "#{warning.message}"

          when Gst::MessageType::ERROR
            error, debug = message.parse_error
            @logger.error "Error in gst pipeline."
            @logger.error "Debugging info: #{debug || 'none'}"
            @logger.error "#{error.message}"

            pluginContext.mainObject.quit
            exit 1

          when Gst::MessageType::EOS #end of stream reached, should not happen for webcam
            @pipeline.stop

            pluginContext.mainObject.quit
            exit 0
          else
            #@logger.debug "New Message: #{message.type.inspect}"
        end
        true
      end

      # @multifilesink.bus.add_watch do |bus, message|
      #   @logger.debug "New Message on multifilesink: #{message.type}"
      # end

      @fileQueue.signal_connect("underrun") do |element|
        #@logger.debug "underrun: #{element}"
        @valve.drop = true
      end

      # GLib::Timeout.add(1000) {
      #   @logger.debug "last message: '#{@tee.last_message}'"
      #   true #removes timeout
      # }
    end

    def show_text(text)
      @logger.info "Showing Text: #{text}"
      @text.text = text
      GLib::Timeout.add(800) {
        @text.text = ''
        false #removes timeout
      }
    end

    def take_picture
      @logger.info "taking picture"
      resetFilename
      @valve.drop = false
    end

    def start
      @logger.info "starting camera"
      @pipeline.play
    end

    def stop
      @logger.info "stopping camera"
      @pipeline.stop
    end

    private

    def resetFilename
      filenamenow = "#{Time.now.utc.iso8601}".gsub(/[:-]/, "_")

      @logger.info "Storing file to location: '#{filenamenow}'"
      @filesink.stop
      @filesink.location = File.join(@pictureBase, "#{filenamenow}.jpeg")
      #@filesink.location = File.join($USERHOME, "Pictures", "#{filenamenow}.jpeg")
      @filesink.play
    end

    def printGstPlugins

      @logger.info "Available GStreamer Plugins:"
      registry = Gst::Registry.get

      registry.plugins.each { |plugin|
        @logger.info " » #{plugin.name} [#{plugin.version}] (#{plugin.description})"
        registry.get_features(plugin.name).each { |feature|
          @logger.info "   » #{feature.name} [#{feature.gtype}]"
        }
      }

    end
  end
end
