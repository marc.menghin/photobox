#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Photobox::Config
  require 'config/defaultableData'
  
	class CategoryData
		def initialize()
			@data = Hash.new()
		end
		
		def [](name)
			if (not @data.has_key?(name))
				@data[name] = DefaultableData.new
			end
			return @data[name]
		end
		
		def []=(name, value)
			self[name] << value
		end
		
		def names
			return @data.keys
		end
	end
end