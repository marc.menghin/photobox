#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Photobox::Config
  require 'config/categoryData'

  $CONFIG_CATEGORY_SEPARATOR = "/"

  class ConfigManager
    attr :config

    def initialize
      @config = Hash.new
      self.loadDefault
    end

    def loadDefault
      #self[Photobox::SubNameSpace::CONFIG_CATEGORY_KEY][Photobox::SubNameSpace::CONFIG_VALUE_KEY] << true
    end

    def [](category)
      if (not @config.has_key?(category))
        @config[category] = CategoryData.new
      end
      return @config[category]
    end

    def categories
      return @config.keys
    end

    def clear
      @config.clear
    end
  end
end