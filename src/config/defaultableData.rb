#!/usr/bin/env ruby
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

module Photobox::Config
  class DefaultableData
    attr_accessor :value, :defaultValue

    def initialize(defaultValue = nil)
      @value = defaultValue
      @defaultValue = defaultValue
    end

    def <<(value)
      @value = value
    end

    def clear
      @value = @defaultValue
    end
  end
end