#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# See LICENSE.txt for permissions.
#
# Contributors:
#  - Marc Menghin (Original Contributor)

#client requires
require 'info'
require 'lib/util'
require 'lib/debugHelper'

module Photobox
  include PhotoboxInfo

  #constants
  CONFIG_CATEGORY_PLUGINS = "Plugins"
  #config file location
  CLIENTCONFIGDIRECTORY = File.join($USERHOME, ".config", CLIENTDIRECTORYNAME)
  #log location (not ideal but works for now)
  CLIENTLOGDIRECTORY = File.join(CLIENTCONFIGDIRECTORY, "logs")

  class PhotoboxMain
    include Util

    @logger = nil

    public

    attr_reader :pluginManager, :configManager

    def initialize
      @configManager = nil
      @pluginManager = nil
      @cmdOptions = nil

      puts "#{Photobox::CLIENTNAME} #{Photobox::CLIENTVERSION}"

      #parse cmd line first for speed
      loadCommandLine

      #additional needen requires (loaded late so cmd line is handeled fast)
      require 'plugins/pluginManager'
      require 'config/configManager'

      #load logging
      loadLogging
      @logger = Logging.logger[self]
      printBasicInformationToLog

      if ($DS)
        self.printEnvData(@logger)
        printCMDStuff
      end

      #load the different parts
      loadConfiguration
      loadPluginManager
    end

    def run
      begin
        startup()
        @pluginManager.run
      rescue SystemExit
        puts "Exiting #{Photobox::CLIENTNAME}."
      ensure
        shutdown()
        puts "Bye, bye ;)"
      end
    end

    private

    def startup
      @logger.info "#{Photobox::CLIENTNAME} Startup"
      @pluginManager.startup(@pluginManager, @configManager, @cmdOptions)

      @pluginManager.printPlugins
      @logger.info "#{Photobox::CLIENTNAME} Startup finished"
    end

    def shutdown
      @logger.info "#{Photobox::CLIENTNAME} Shutdown"
      @pluginManager.shutdown

      @pluginManager.printPlugins
      @logger.info "#{Photobox::CLIENTNAME} Shutdown finished"
    end

    def loadCommandLine
      @cmdOptions = {}
      @cmdFiles = []
      optparse = OptionParser.new do |opts|
        # Set a banner, displayed at the top
        # of the help screen.
        opts.banner = "Usage: sopran [options] file1 file2 ..."
        # Define the options, and what they do
        @cmdOptions[:profile] = nil
        opts.on('-p', '--profile', 'Set a profile to start (default: trayicon)') do |profile|
          @cmdoptions[:profile] = profile
        end
        @cmdOptions[:verbose] = false
        opts.on('-v', '--verbose', 'Output more information') do
          @cmdOptions[:verbose] = true
        end
        @cmdOptions[:quick] = false
        opts.on('-d', '--debug', 'Increases log output') do
          @cmdOptions[:debug] = true
          $DS = true
        end

        @cmdOptions[:logfile] = nil
        opts.on('-l', '--logfile FILE', 'Write log to FILE') do |file|
          @cmdOptions[:logfile] = file
        end

        opts.separator ""
        opts.separator "Common options:"

        # This displays the help screen, all programs are
        # assumed to have this option.
        opts.on_tail('-h', '--help', 'Display this screen') do
          puts opts
          exit
        end

        # Another typical switch to print the version.
        opts.on_tail("--version", "Show version") do
          puts OptionParser::Version.join('.')
          exit
        end
      end

      optparse.program_name = "#{Photobox::CLIENTNAME}"
      optparse.version = "#{Photobox::CLIENTVERSION}"
      optparse.release = ""

      # Parse the command-line. Remember there are two forms
      # of the parse method. The 'parse' method simply parses
      # ARGV, while the 'parse!' method parses ARGV and removes
      # any options found there, as well as any parameters for
      # the options. What's left is the list of files to resize.
      begin
        optparse.parse!
      rescue
        puts ""
        puts "Unknown Option."
        puts ""
        puts optparse.help()
        exit
      end

      #get files from command-line
      @cmdOptions[:files] = []
      ARGV.each do |file|
        @cmdOptions[:files] << file
      end
      if not @cmdOptions[:files].empty?
        @cmdOptions[:profile] = "cmdline"
      end
    end

    def printCMDStuff
      @logger.info "CMDLine Options:"
      @cmdOptions.each do |key, value|
        @logger.info " » #{key} => #{value}"
      end
    end

    def loadConfiguration
      #create config location if not already there
      if (not File.exist?(CLIENTCONFIGDIRECTORY))
        @logger.info "Config directory not found, will try to create it."
        @logger.info "New Config directory: #{CLIENTCONFIGDIRECOTRY}"
        FileUtils.mkdir_p(CLIENTCONFIGDIRECTORY)
      end

      @configManager = Config::ConfigManager.new
    end

    def loadPluginManager
      #load plugins
      @pluginManager = Plugins::PluginManager.new

      #add default paths
      @pluginManager << File.join($CLIENTPATH, "plugins")
      @pluginManager << File.join(CLIENTCONFIGDIRECTORY, "plugins")

      #search and load plugins
      @pluginManager.loadPlugins()

      if ($DS)
        self.printRubySearchPaths(@logger)
        @pluginManager.printPlugins()
      end
    end

    def loadLogging
      require 'logging'

      #create log directory
      if (not File.exist?(CLIENTLOGDIRECTORY))
        puts "Log directory not found, will try to create it."
        puts "Log directory: #{CLIENTLOGDIRECTORY}"
        FileUtils.mkdir_p(CLIENTLOGDIRECTORY)
      end

      #setup colors
      Logging.color_scheme('bright',
                           :levels => {
                               :info => :green,
                               :warn => :yellow,
                               :error => :red,
                               :fatal => [:white, :on_red]
                           },
                           :date => :blue,
                           :logger => :cyan,
                           :message => :magenta
      )

      #config root logger used by all loggers
      Logging.logger.root.level = :debug
      Logging.logger.root.add_appenders([
                                           # Logging.appenders.file('file',
                                           #                        :filename => File.join(CLIENTLOGDIRECTORY, 'photobox.log'),
                                           #                        :layout => Logging.layouts.pattern(:pattern => '%d %-5l %c: %m\n')),
                                            Logging.appenders.stdout('stdout',
                                                                     :layout => Logging.layouts.pattern(:pattern => '%6r %-4l %c{1}: %m\n',
                                                                                                        :color_scheme => 'bright'))
                                        ])

      if @cmdOptions[:logfile]
        Logging.logger.root.add_appenders(Logging.appenders.file(@cmdOptions[:logfile]))
      end
    end

    def printBasicInformationToLog
      #print some basic information to the log file
      @logger.info "#{Photobox::CLIENTNAME} #{Photobox::CLIENTVERSION}"
      @logger.info "#{$CLIENTPATH}"
      @logger.info "Log: #{CLIENTLOGDIRECTORY}"
      @logger.info "Config: #{CLIENTCONFIGDIRECTORY}"
      @logger.info "UserHome: #{$USERHOME}"
      @logger.info((Time.new).strftime("Time: %Y-%m-%d %H:%M:%S"))
    end
  end
end
